import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

var user = {
    name: 'Andrew',
    age: 26,
    location: 'Philadelphia'
};

function getLocation(location){
    if(location){
        return <p>Location: {location}</p>
    }
}

var templateTwo = (
    <div>
        <h1>{user.name?user.name:'Anonymous'}</h1>
        {(user.age && user.age>=18) && <p>{user.age}</p>}
        {getLocation(user.location)}
    </div>
);

var app = {
    title : 'Indecision App',
    subtitle : 'subtitle',
    options: [ 'One']
};

var template = (
    <div>
        <h1>{app.title}</h1>
        { app.subtitle && <p>{app.subtitle}</p>}
        <p>{app.options.length>0? <p>Here are your options:
                <ol>
                <li>Item One</li>
                <li>Item Two</li>
                </ol></p>
            :'No options'}</p>
    </div>
)

ReactDOM.render(template, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
