import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import Home from './home'
import Users from './users'
import Contact from './contact'
import Notfound from './notfound'
import {
    Route,
    NavLink,
    BrowserRouter as Router,
    Switch,
} from 'react-router-dom'

const routing = (
    <Router>
        <ul>
            <li>
                <NavLink exact activeClassName="active" to="/">
                    Home
                </NavLink>
            </li>
            <li>
                <NavLink activeClassName="active" to="/users">
                    Users
                </NavLink>
            </li>
            <li>
                <NavLink activeClassName="active" to="/contact">
                    Contact
                </NavLink>
            </li>
        </ul>
        <hr />
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/users" component={Users} />
            <Route path="/contact" component={Contact} />
            <Route component={Notfound} />
        </Switch>
    </Router>
);


ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
