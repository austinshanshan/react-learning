import React from 'react';
import './App.css';
import Form from './Form.js'

function App() {
  return (
    <div className="App">
      <header className="App-header">
          Calculator
      </header>
        <Form></Form>
    </div>
  );
}

export default App;
