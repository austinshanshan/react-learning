import React from 'react';
import './Form.css';
const axios = require('axios');


class Form extends React.Component {
  state = { companyName: '' };
 
  handleSubmit = async (event) => {
    event.preventDefault();
    const resp = await axios.get(`https://api.github.com/users/${this.state.companyName}`);
    console.log(resp.data);
    this.setState({ companyName: '' });
  };

	render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <span className="formtext">&#x3C;Form /&#x3E;</span>
                    <input 
                        type="text"
                        value={this.state.companyName}
                        onChange={event=>this.setState({companyName:event.target.value})}
                        placeholder="Enter Company Name"
                    />
                    <button>Go!</button>
            </form>
        );
  }
}

export default Form;