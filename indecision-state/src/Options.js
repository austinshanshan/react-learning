import React from 'react';


class Options extends React.Component {
    render() {
        return (
            <div>
                <button onClick={this.props.handleDeleteOptions}>Remove All</button>
                {
                    this.props.options.map((option) => <option key={option} optionText={option}>{option}</option>)
                }
            </div>
        );
    }
}

export default Options;