import React from 'react';

const Options = (props) => {
    return (
        <div>
            <button onClick={props.handleDeleteOptions}>Remove All</button>
            {
                props.options.map((option) => <option key={option} optionText={option}>{option}</option>)
            }
        </div>
    )
}


export default Options;