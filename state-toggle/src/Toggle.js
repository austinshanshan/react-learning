import React from 'react';

class Toggle extends React.Component {

    constructor (props) {
        super(props);
        this.state = {visibility: false};
        this.toggleVisibility = this.toggleVisibility.bind(this);
    }

    toggleVisibility() {
        // this.setState({
        //     visibility: !this.visibility
        // });

        this.setState((prevState) => {
            return {visibility: ! prevState.visibility}
        });
    }

    render()  {
        return (
            <div>
                <h1>Visibility Toggle</h1>
                <button onClick={this.toggleVisibility}>
                    {this.state.visibility? 'Hide details':'Show details'}
                </button>
                {this.state.visibility && (
                    <div>
                        <p>Hey. These are some details you can now see!</p>
                    </div>
                )}
            </div>
        );
    }
}

export default Toggle;