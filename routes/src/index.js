import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter, Route} from "react-router-dom";

const HelpPage = () => {
    return <p>Help Page</p>;
};

const ExpensePage = () => {
    return <p>Expense Page</p>;
};

const routes =  (
    <BrowserRouter>
        <div>
            <Route path="/" component={ExpensePage} exact={true}/>
            <Route help="/help" component={HelpPage}/>
        </div>
    </BrowserRouter>
);



ReactDOM.render(routes, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
