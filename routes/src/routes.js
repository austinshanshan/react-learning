import ExpenseDashboardPage from "./ExpenseDashboardPage";
import AddExpensePage from "./AddExpensePage";
import EditExpensePage from "./EditExpensePage";
import HelpPage from "./HelpPage";
import React from 'react';
import {BrowserRouter, Route} from "react-router-dom";


function routes() {
    return <ExpenseDashboardPage/>;
    {/*<BrowserRouter>*/}
        {/*<div>*/}
            {/*<Route path="/" component={ExpenseDashboardPage} exact={true}/>*/}
            {/*/!*<Route path="/create" component={AddExpensePage}/>*!/*/}
            {/*/!*<Route path="/edit" component={EditExpensePage}/>*!/*/}
            {/*/!*<Route path="/help" component={HelpPage}/>*!/*/}
        {/*</div>*/}
    {/*</BrowserRouter>;*/}
}

export default routes;